from django.forms import ModelForm
from django import forms
from .models import Task


class CreateTaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
        widgets = {
            "start_date": forms.DateTimeInput(
                attrs={"type": "datetime-local"}
            ),
            "due_date": forms.DateTimeInput(attrs={"type": "datetime-local"}),
        }
